FROM java:8-alpine

RUN mkdir -p /app /app/resources

WORKDIR /app

COPY target/uberjar/*-standalone.jar .

CMD java -jar rest-demo-latest-standalone.jar

EXPOSE 3000